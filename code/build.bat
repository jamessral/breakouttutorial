@echo off

IF NOT DEFINED LIB (call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x86_amd64)

IF NOT EXIST ..\build mkdir ..\build
pushd ..\build
del *.pdb > NUL 2> NUL
echo WAITING FOR PDB > lock.tmp

cl -nologo -Zi -FC ..\code\win32_platform.c /link user32.lib gdi32.lib -incremental:no -opt:ref

del lock.tmp
del *.obj
REM IF EXIST .\win32_platform.exe .\win32_platform.exe
popd
